<p align="center">

![Skraper Logo](logo.png)

</p>

## Francais
> Vous avez un problème, une suggestion pour Skraper ? 
> 
Vous êtes au bon endroit ! Ce répertoire git a pour unique objectif de recevoir les "issues" (problèmes) de Skraper. Elle permettent aux développeurs du projet (@Bkg2k actuellement, et @Redblueflame récamment) d'améliorer constament le logiciel.
### Informations importantes
 Avant de publier une nouvelle issue, merci de vérifier que vous suivez le template fourni avec le type du problème ainsi que de fournir tous les éléments demandés. Cela nous permettera de pouvoir travailler plus vite sur les problèmes ou les suggestions.

 Si vous avez tout lu, vous pouvez créer une nouvelle issue en cliquant sur le lien ci dessous:

[Créer une nouvelle issue !](https://gitlab.com/screenscraper/skraper-issues/issues/new)

/!\ Il vous faut un compte GitLab afin créer une issue. Si vous ne pouvez pas, merci de laisser un message sur
[le discord](https://discord.gg/aCsgSU) avec la description du problème.
## English
> Do you have any problem or a suggestion with Skraper ?

If the response is yes, then you're at the right place ! This repository has for only mission to gather issues and suggestions for Skraper. They will be used by the developers of the project (@Bkg2k since the beginning, and @Redblueflame recently) to make Skraper a better software.
### Important informations
Before publishing anything, please check that your issue matches the template of your type of issue (Suggestion / Bug), and that you have attached everything needed with the format.

If you have read everything, you can now create your first issue !

[Create an issue !](https://gitlab.com/screenscraper/skraper-issues/issues/new)

/!\ You need a GitLab account to post an issue on this website. If you don't want or cannot create an account, or you have a question, you can do so in the Discord server, available [here](https://discord.gg/aCsgSU).
