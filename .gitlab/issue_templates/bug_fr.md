# Skraper Bug
 Cette issue est un bug avec Skraper

 Merci d'enlever tout ce qui est entre crochets [] avant de publier l'issue.
### Description
[Description de l'erreur en quelques mots]
### Etapes de reproduction
[Explication étape par étape de comment reproduire le bug]
- Ceci est la première étape
- Ceci est la seconde étape
### Résultat attendu
[Quel est le résultat attendu si le bug était abscent.]
### Résultat actuel
[Que ce passe-t-il ?]

## Environement
Version de Skraper :

OS: [Windows / Mac OS / Linux]

Version de l'OS: [On windows, WIN + R, then `winver`]

[`Cette section est pour les utilisateurs de Linux`]

Distribution Linux: 

Utilisez-vous Mono ou Wine ?

Version de Wine / Mono:

## Attachments

Lien Hastebin / Pastebin du log du logiciel: 

Si vous avez des screenshots, merci de les ajouter au rapport en utilisant la fonctionnalité incluse dans GitLab.

Merci de rajouter toute autre information sur l'environement que vous jugez nécessaire ici.