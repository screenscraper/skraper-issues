# Skraper Bug
 This issue is a bug with Skraper.

 You need to remove anything between []
### Description
[Please describe the bug here in a few words]
### Steps to reproduce
[Please describe here how to reproduce the bug in a bulletpoint list]
- This is the first step
- This is the second step
### Expected result
[What is the exepected result ? What is expected if everything works fine ?]
### Actual result
[What happens ?]

## Environment
Skraper version :

OS: [Windows / Mac OS / Linux]

OS version: [On windows, WIN + R, then `winver`]

[`This section is for Linux`]

Distribution: 

Are you using Mono or Wine ?

Version of Wine / Mono:

## Attachments

Hastebin / Pastebin link of the log file [Please check if it's activated]: 

If you have any screenshots, please add them to the issue using GitLab file upload feature.

Please add here any other useful information for your specific issue here.